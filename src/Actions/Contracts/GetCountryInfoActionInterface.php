<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 * This software is licensed under GNU AGPLv3 - https://www.gnu.org/licenses/agpl-3.0.en.html.
 * For commercial use you can obtain a limited commercial license from the author.
 *
 *
 */

namespace Inforisorse\CountryUtils\Actions\Contracts;

use stdClass;

interface GetCountryInfoActionInterface
{
    public function handle(): array;
}
