<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 */
declare(strict_types=1);

namespace Inforisorse\CountryUtils\Actions;

use GuzzleHttp\Client;
use Inforisorse\CountryUtils\Actions\Contracts\GetCountryInfoActionAbstract;

class GetInternationalPhoneCountryCodes extends GetCountryInfoActionAbstract
{
    public static function make(): GetInternationalPhoneCountryCodes
    {
        return new self(new Client());
    }
    protected function getEndpoint(): string
    {
        return 'http://country.io/phone.json';
    }
}
