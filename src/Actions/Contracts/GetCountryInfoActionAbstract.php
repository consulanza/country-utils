<?php
/*
 * Copyright (c) 2024. Amedeo Lanza - ConsuLanza Informatica.
 */

declare(strict_types=1);

namespace Inforisorse\CountryUtils\Actions\Contracts;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use stdClass;

abstract class GetCountryInfoActionAbstract implements GetCountryInfoActionInterface
{
    public function __construct(
        protected readonly Client $client
    )
    {}

    public abstract static function make(): GetCountryInfoActionAbstract;

    /**
     * @throws GuzzleException
     */
    public function handle(): array
    {
        $headers = [
            'Accept' => 'application/json',
        ];
        $endpoint = $this->getEndpoint();

        $result = $this->client->request(
            'GET',
            $endpoint,
            [
                'headers' => $headers,
            ]
        );

        $content = $result->getBody()->getContents();
        return get_object_vars(json_decode($content));
    }



    protected abstract function getEndpoint(): string;
}
